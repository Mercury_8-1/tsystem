package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null)
            throw new IllegalArgumentException();
        if (x.size() > y.size())
            return false;
        List<Object> first = new ArrayList<Object>(x);
        List<Object> second = new ArrayList<Object>(y);
        int size = first.size();
        for (int i = 0; i < second.size(); i++) {
            if (!first.contains(second.get(i))) {
                second.remove(i);
            }
        }
        if (second.size() > size) {
            for (int i = 0; i < size; i++) {
                if (second.get(i) != first.get(i)) {
                    second.remove(i);
                    if (second.size() < size) {
                        return false;
                    }
                }
            }
        }
        if (second.size() == size) {
            return second.equals((first));
        }
        return true;
    }

    public static void main(String[] args){
        Subsequence s = new Subsequence();
        boolean b = s.find(null, new ArrayList());
        boolean c = s.find(new ArrayList(), null);
        System.out.println(b);
        System.out.println(c);
    }
}