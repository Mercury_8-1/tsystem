package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException();
        if (POSSIBLE_PYRAMID_SIZES.containsKey((long) inputNumbers.size())) {
            Collections.sort(inputNumbers);
            builder(inputNumbers);
        }
        else throw new CannotBuildPyramidException();
        return result;
    }

    private int[][] result;
    private static final LinkedHashMap<Long, Long> POSSIBLE_PYRAMID_SIZES = possiblePyramidSizes();

    public PyramidBuilder() {/*
        System.out.println("Total number of possible pyramids: \t" + POSSIBLE_PYRAMID_SIZES.size());
        System.out.print("Possible pyramids sizes: \t\t\t");
        for (Map.Entry<Long, Long> entry : POSSIBLE_PYRAMID_SIZES.entrySet())
            System.out.print(entry.getKey() + "(" + entry.getValue() + ") ");
        System.out.println();
        starter();*/
    }

    private void starter() {
        List<Integer> data;
        for (int i = 0; i < 10; i++) {
            data = randomDataGenerator();
            if (POSSIBLE_PYRAMID_SIZES.containsKey((long) data.size())) {
                Collections.sort(data);
                System.out.print(data.size() + " elements (" +
                        POSSIBLE_PYRAMID_SIZES.get((long) data.size()) + "-layer pyramid) \t\t");
                for (Integer num : data)
                    System.out.print(num + " ");
                System.out.println();
                builder(data);
            }
        }
    }

    private void builder(List<Integer> data) {
        long size = POSSIBLE_PYRAMID_SIZES.get((long) data.size());
        result = new int[(int) size][(int) size * 2 - 1];
        List<Integer> buffer = new ArrayList<Integer>((int) size);
        int row = 0, filler;
        int width = (int) size * 2 - 1;
        int indent = (width - row) / 2;
        for (int i = 0; i < data.size(); i++) {
            if (buffer.size() <= row) {
                buffer.add(data.get(i));
            }
            else {
                filler = 0;
                for (Integer value : buffer) {
                    result[row][indent + filler] = value;
                    filler = filler + 2;
                }
                row++;
                indent--;
                buffer.clear();
                buffer.add(data.get(i));
            }
        }
        filler = 0;
        for (Integer value : buffer) {
            result[row][indent + filler] = value;
            filler = filler + 2;
        }
        // print2DDArray();
    }

    private static LinkedHashMap<Long, Long> possiblePyramidSizes() {
        LinkedHashMap<Long, Long> data = new LinkedHashMap<Long, Long>();
        long value = 1, step = 1;
        while (value < 700) {
            data.put(value, step);
            step++;
            value = value + step;
        }
        return data;
    }

    private List<Integer> randomDataGenerator() {
        Random r = new Random();
        int arrayLength = r.nextInt(100);
        List<Integer> generatedData = new ArrayList<Integer>(arrayLength);
        for (int i = 0; i < arrayLength; i++) {
            generatedData.add(r.nextInt(100) + 1);
        }
        return generatedData;
    }

    private void print2DDArray() {
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                System.out.printf("%3d", result[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        PyramidBuilder pyramid = new PyramidBuilder();
    }
}