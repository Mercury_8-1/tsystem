package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        try {
            Parser p = new Parser(statement);
            Calculations c = new Calculations(p.elements);
            return c.result;
        } catch (ArithmeticException e) {
            return null;
        }
    }

    private static String operators = "+-/*";

    public Calculator() {
    }

    private Calculator(String expr) {
        Parser p = new Parser(expr);
        new Calculations(p.elements);
    }

    private class Parser {

        private String error = "";
        private List<String> elements = new ArrayList<String>();
        private StringBuilder sb = new StringBuilder();
        private int bracketCounter = 0;

        private Parser(String expr) {
            parser(expr);
            unaryOperators();
            // print();
        }

        private void parser(String expr) {
            for (Character chr : String.valueOf(expr).toCharArray()) {
                if (Character.isDigit(chr)) {
                    if (sb.length() == 0 && elements.size() > 0)
                        if (elements.get(elements.size() - 1).equals(")")) {
                            elements.add("*");
                        }
                    sb.append(chr);
                }
                else if (chr == '.'/* || chr == ','*/)
                    addDot(chr);
                else if (operators.contains(String.valueOf(chr)))
                    addOperator(chr);
                else if (chr == '(' || chr == ')')
                    addBracket(chr);
                else if (Character.isSpaceChar(chr))
                    continue;
                else incorrectData("Incorrect symbol: " + "'" + chr + "'");
            }
            if (elements.size() == 0) {
                error = null;
                throw new ArithmeticException("Empty statement.");
            }
            if (sb.length() > 0) {
                if (sb.charAt(sb.length() - 1) != '.' || sb.charAt(sb.length() - 1) != ',')
                    elements.add(sb.toString());
            }
            if (bracketCounter > 0)
                incorrectData("Incorrect brackets.");
        }

        private void addDot(char chr) {
            if (sb.length() > 0 && !sb.toString().contains(".") && !sb.toString().contains(","))
                sb.append(chr);
            else incorrectData("Incorrect '" + chr + "' position.");
        }

        private void addOperator(char chr) {
            if (sb.length() > 0)
                if (sb.charAt(sb.length() - 1) != '.' && sb.charAt(sb.length() - 1) != ',') {
                    elements.add(sb.toString());
                    sb.setLength(0);
                } else incorrectData("Incorrect operator position: '.' or ',' is followed by " + chr);
            if (!elements.isEmpty() && !operators.contains(elements.get(elements.size() - 1)))
                elements.add(String.valueOf(chr));
            else if (elements.isEmpty())
                elements.add(String.valueOf(chr));
            else incorrectData("Incorrect operator order: " +
                        elements.get(elements.size() - 1) + " followed by " + chr);
        }

        private void addBracket(char chr) {
            if (sb.length() > 0)
                if (sb.charAt(sb.length() - 1) != '.' || sb.charAt(sb.length() - 1) != ',') {
                    elements.add(sb.toString());
                    sb.setLength(0);
                } else incorrectData("Incorrect bracket order.");
            if (chr == '(') {
                if (elements.size() > 0)
                    if (!operators.contains(elements.get(elements.size() - 1)) &&
                            !elements.get(elements.size() - 1).equals("(")) {
                        elements.add("*");
                    }
                elements.add(String.valueOf(chr));
                bracketCounter++;
            } else {
                if (bracketCounter > 0) {
                    if (!operators.contains(elements.get(elements.size() - 1)) ||
                            !elements.get(elements.size() - 1).equals(".") ||
                            !elements.get(elements.size() - 1).equals(",")) {
                        elements.add(String.valueOf(chr));
                        bracketCounter--;
                    }
                } else incorrectData("Incorrect bracket order.");
            }
        }

        private void unaryOperators() {
            for (int i = 0; i < elements.size(); i++) {
                if (elements.get(i).equals("+") || elements.get(i).equals("-")) {
                    if (i == elements.size() - 1 || elements.get(i + 1).equals(")"))
                        incorrectData("Operator as last element.");
                    else {
                        if (i > 0) {
                            if (elements.get(i - 1).equals("(")) {
                                elements.set(i, elements.get(i) + elements.get(i + 1));
                                elements.remove(i + 1);
                            }
                        }
                        else {
                            elements.set(i, elements.get(i) + elements.get(i + 1));
                            elements.remove(i + 1);
                        }
                    }
                }
            }
        }

        private void incorrectData(String str) {
            error = null;
            throw new ArithmeticException(str);
        }

        private void print() {
            for (String str : elements)
                System.out.print(str + " ");
            System.out.println();
        }
    }

    class Calculations {

        private String result;

        Calculations(List<String> elements) {
            bracketFinder(elements);
            getResult(elements);
            // print(result);
        }

        private void bracketFinder(List<String> elements) {
            int lastOpener = -1, firstCloser = -1;
            for (int i = 0; i < elements.size(); i++) {
                if (elements.get(i).equals(")")) {
                    firstCloser = i;
                    break;
                }
            }
            if (firstCloser != 0) {
                for (int i = firstCloser; i >= 0; i--) {
                    if (elements.get(i).equals("(")) {
                        lastOpener = i;
                        break;
                    }
                }
            }
            if (lastOpener != -1 && firstCloser != -1)
                bracketOpener(lastOpener, firstCloser, elements);
        }

        private void bracketOpener(int opener, int closer, List<String> elements) {
            int operatorIndex = -1;
            for (int i = opener; i <= closer; i++) {
                if (operators.contains(elements.get(i))) {
                    if (operationPriority(elements.get(i)) == 2) {
                        calculations(elements.get(i - 1), elements.get(i + 1), elements.get(i), i, elements);
                        break;
                    }
                    else if (operatorIndex == -1)
                        operatorIndex = i;
                }
            }
            if (operatorIndex != -1) {
                calculations(elements.get(operatorIndex - 1), elements.get(operatorIndex + 1),
                        elements.get(operatorIndex), operatorIndex, elements);
            }
            else if (closer - opener < 3) {
                elements.remove(closer);
                elements.remove(opener);
            }
            bracketFinder(elements);
        }

        private void getResult(List<String> elements) {
            int operatorIndex = -1;
            for (int i = 0; i < elements.size(); i++) {
                if (operators.contains(elements.get(i))) {
                    if (operationPriority(elements.get(i)) == 2) {
                        calculations(elements.get(i - 1), elements.get(i + 1), elements.get(i), i, elements);
                        getResult(elements);
                        return;
                    }
                    else if (operatorIndex == -1)
                        operatorIndex = i;
                }
            }
            if (operatorIndex != -1) {
                calculations(elements.get(operatorIndex - 1), elements.get(operatorIndex + 1),
                        elements.get(operatorIndex), operatorIndex, elements);
            }
            if (elements.size() > 1)
                getResult(elements);
            else
                round(elements.get(0));
        }

        private void calculations(String operand1, String operand2, String operator, int index, List<String> elements) {
            String res = "";
            if (operator.equals("+"))
                res = String.valueOf(Double.parseDouble(operand1) + Double.parseDouble(operand2));
            else if (operator.equals("-"))
                res = String.valueOf(Double.parseDouble(operand1) - Double.parseDouble(operand2));
            else if (operator.equals("*"))
                res = String.valueOf(Double.parseDouble(operand1) * Double.parseDouble(operand2));
            else if (operator.equals("/")) {
                if (operand2.equals("0.0")) {
                    result = null;
                    throw new ArithmeticException("Division by null");
                }
                res = String.valueOf(Double.parseDouble(operand1) / Double.parseDouble(operand2));
            }
            elements.set(index, res);
            elements.remove(index + 1);
            elements.remove(index - 1);
        }

        private int operationPriority(String operator) {
            if (operator.equals("+") || operator.equals("-"))
                return 1;
            else
                return 2;
        }

        private void round(String str) {
            BigDecimal bd = BigDecimal.valueOf(Double.parseDouble(str));
            bd = bd.setScale(4, RoundingMode.HALF_UP).stripTrailingZeros();
            result = bd.toPlainString();
        }

        private void print(String result) {
            System.out.println(result);
        }
    }

    public static void main(String[] args) {
        args = new String[1];
        args[0] = "10/(5-5)";
        //args[1] = "(4/1)";
        //args[2] = "13+2*2/10.5";
        if (args.length > 0) {
            for (String expr : args) {
                new Calculator(expr);
            }
        }
    }
}